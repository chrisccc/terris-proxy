def run(player):
    if not player.attributes.isDelayed():
        if not player.affects.haste:
            if player.magic.spell == "Haste":
                player.do("t");
            elif player.magic.spell is None:
                player.do("cast 61");