class Combat:
    def __init__(self):
        self.advanced = {};

    def update(self, topic, arguments):
        if topic is "combatAdvance":
            self.enemyAdvanced(arguments["advancee"], arguments["advancer"]);

        if topic is "combatRetreat":
            self.retreated(arguments["retreater"]);

    def enemyAdvanced(self, advancee, advancer):
        if advancee in self.advanced:
            self.advanced[advancee].append(advancer);
        else:
            self.advanced[advancee] = [advancer];

    def retreated(self, retreater):
        if retreater in self.advanced:
            self.advanced[retreater] = None;

    def isAdvanced(self, who):
        return self.advanced[who] and len(self.advanced[who]) > 0;