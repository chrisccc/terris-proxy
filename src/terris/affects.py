class Affects:
    def __init__(self):
        self.flying = False;
        self.haste = False;

    def update(self, topic, arguments):
        if topic is "affectStart":
            self.updateAffect(arguments["affect"], True);
        elif topic is "affectEnd":
            self.updateAffect(arguments["affect"], False);

    def updateAffect(self, affect_key, value):
        if affect_key == 'F':
            self.flying = value;

        elif affect_key == 'H':
            self.haste = value;