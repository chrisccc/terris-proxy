class Attributes:
    def __init__(self):
        self.delay = 0.0;
        self.max_health = 0;
        self.health = 0;
        self.max_mana = 0;
        self.mana = 0;

        self.gold = 0;
        self.exp = 0.0;
        self.guild_exp = 0.0;
        self.temple_exp = 0.0;
        self.city_exp = 0.0;

    def update(self, topic, arguments):
        if topic is "attributeUpdate":
            eventType = arguments["type"];

            if eventType is "gold":
                self.gold = arguments["gold"];
                self.exp = arguments["exp"];
                self.delay = float(arguments["delay"]) / 18;
                self.guild_exp = arguments["guild_exp"];
                self.temple_exp = arguments["temple_exp"];
                self.city_exp = arguments["city_exp"];
            elif eventType is "status":
                self.health = arguments["health"];
                self.max_health = arguments["max_health"];
                self.mana = arguments["mana"];
                self.max_mana = arguments["max_mana"]
        elif topic is "delay":
            self.delay = float(arguments["time"]);
        elif topic is "delayExpired":
            self.delay = 0.0;

    def isDelayed(self):
        return self.delay > 0.0;