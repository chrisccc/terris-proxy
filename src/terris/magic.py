class Magic:
    def __init__(self):
        self.spell = None;
        self.name = None;

    def update(self, topic, arguments):
        if topic is "spellCast":
            self.spell = arguments["spell"];
        elif topic is "spellFail":
            self.spell = None;
        elif topic is "spellTarget":
            if arguments["caster"] == self.name:
                self.spell = None;

    def isSpellCast(self):
        return self.spell is not None;