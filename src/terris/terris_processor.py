import re;
import threading;

class TerrisProcessor:

    ################################################################################
    # Processor constructor.  Exists to declare objects that should exist over the
    # lifetime of the instance that are used through out.
    ################################################################################
    def __init__(self, broker, commands):
        self.broker = broker;
        self.commands = commands;

        self.shouldParseRoom = False;
        self.roomBuffer = [];

        self.eventRegexes = self.makeRegexes();

        self.delayTimer = None;
        self.escapeHandler = None;


    ################################################################################
    # Factory method that exists to provide the regexes used to identify events
    # happening in game.
    ################################################################################
    def makeRegexes(self):
        return {
            r"\[L\]\^CTerris Comings and goings: \^N \^Y(.*)\^N has logged in": self.onLogin,

            r"You are unbalanced.  Wait \^Y(.*)\^N seconds more...": self.onDelay,

            r"Thanks for playing! Character saved ...": self.onQuit,

            r"You chant the words of a \^C(.*)\^N spell and attempt to control the power": self.onSpellCast,

            r"Your \^C(.*)\^N spell fails for some reason. Perhaps you should become a knight?": self.onSpellFail,
            r"As you complete the casting of your \^C(.*)\^N spell, your hair stands on end, you feel a chill, but nothing happens...": self.onSpellFail,
            r"Just when you think you've completed the casting of your \^C(.*)\^N spell, you see a small tadpole appear in your hand...": self.onSpellFail,
            r"As you complete the casting of your \^C(.*)\^N spell, you notice daisies falling from the sky...": self.onSpellFail,
            r"As you complete the casting of your ^C(.*)^N spell, you realize you've cast an air freshener spell by mistake!": self.onSpellFail,

            r"\^C(.*)\^N gestures towards (.*)": self.onSpellTarget,
            r"\^C(.*)\^N gestures at \^R(.*)\^N...": self.onSpellTarget,

            r"\^R(.*)\^N advances towards \^C(.*)\^N": self.onAdvance,

            r"\^C(.*)\^N retreats": self.onRetreat,

            r"\^R(.*)\^N .* in!": self.onEnemySpawn,
            r"\^R(.*)\^N (.*) in \^Yfrom (.*)": self.onEnemySpawn,

            r"\^R(.*)\^N is slain!": self.onEnemyKill
        };


    ################################################################################
    # Callback method that exists so we can look for custom commands in data
    # travelling from the client to the host.
    ################################################################################
    def onClientToHost(self, data):
        if data is not None:
            command = data.strip();

            cmd = self.commands.get(command, None);

            if cmd is not None:
                cmd.execute();
            else:
                return data;


    ################################################################################
    # Callback method that exists so we can parse data from the game and attempt to
    # deduce if certain events happen.
    ################################################################################
    def onHostToClient(self, data):
        if data is not None:
            lines = data.split("\n");

            for line in lines:

                if self.shouldParseRoom:
                    self.parseRoom(line);
                else:
                    self.parseLine(line);

        return data;


    ################################################################################
    # Method that exists to parse a line of data received from the game in the
    # context of a room.
    ################################################################################
    def parseRoom(self, line):
        if len(line) > 1:
            if ord(line[0]) == 27:
                if line[1] == 'e':
                    self.shouldParseRoom = False;

                    for roomLine in self.roomBuffer:
                        print(roomLine);

                    # TODO: Parse Room

                    self.roomBuffer = [];

                    return;

            self.roomBuffer.append(line);


    ################################################################################
    # Method that exists to parse a line of data received from the game and call
    # an appropriate method to parse received data.
    ################################################################################
    def parseLine(self, line):
        if len(line) > 1:
            if ord(line[0]) == 27:
                self.escapeHandler = {
                    'E': self.onRoomEscape,
                    'Q': self.onGoldEscape,
                    'S': self.onStatusEscape,
                    'A': self.onAffectNewEscape,
                    'a': self.onAffectEndEscape
                }.get(line[1], None);

            if self.escapeHandler is not None:
                self.escapeHandler(line);
                self.escapeHandler = None;
            else:
                for key, callback in self.eventRegexes.iteritems():
                    result = re.match(key, line);

                    if result is not None:
                        callback(result);

                        break;


    ################################################################################
    # Method that exists to flag when we receive a room start escape code so we know
    # we need to do some special parsing.
    ################################################################################
    def onRoomEscape(self, data):
        self.shouldParseRoom = True;


    ################################################################################
    # Method that exists to flag when we receive a end start escape code so we know
    # we can resume normal parsing.
    ################################################################################
    def onRoomEndEscape(self, data):
        self.shouldParseRoom = False;


    ################################################################################
    # Method that exists to raise events related to the gold escape and to start a
    # timer if needed so we can raise an event that lets them know when the timer
    # has expired
    ################################################################################
    def onGoldEscape(self, data):
        elements = data[2:].split(",");

        if len(elements) != 6:
            print("Unexpected number of elements in gold escape, data: {0}".format(data))
        else:
            gold = elements[0];
            exp = elements[1];
            delay = elements[2];
            guild_exp = elements[3];
            temple_exp = elements[4];
            city_exp = elements[5];

            self.broker.publish("attributeUpdate", {
                "type": "gold",
                "gold": gold,
                "exp": exp,
                "delay": delay,
                "guild_exp": guild_exp,
                "temple_exp": temple_exp,
                "city_exp": city_exp
            });

            if not self.delayTimer and delay > 0:
                self.delayTimer = threading.Timer(float(delay) / 18, self.onDelayExpired);
                self.delayTimer.start();


    ################################################################################
    # Method that exists to raise events related to the status escape.
    ################################################################################
    def onStatusEscape(self, data):
        elements = data[4:].split();

        if len(elements) != 8:
            print("Unexpected number of elements in status escape, data: {0}".format(data));
        else:
            health = elements[0];
            max_health = elements[1];
            mana = elements[2];
            max_mana = elements[3];

            self.broker.publish("attributeUpdate", {
                "type": "status",
                "health": health,
                "max_health": max_health,
                "mana": mana,
                "max_mana": max_mana
            });


    ################################################################################
    # Method that exists to raise events related to new affects.
    ################################################################################
    def onAffectNewEscape(self, data):
        affect = data[2:3]

        self.broker.publish("affectStart", { 'affect': affect });


    ################################################################################
    # Method that exists to raise events related to the end of affects.
    ################################################################################
    def onAffectEndEscape(self, data):
        affect = data[2:3]

        self.broker.publish("affectEnd", { 'affect': affect });


    ################################################################################
    # Method that exists to raise events related to logins
    ################################################################################
    def onLogin(self, match):
        name = match.group(1);

        self.broker.publish("login", { "name": name });


    ################################################################################
    # Method that exists to raise events when a spell is cast.
    ################################################################################
    def onSpellCast(self, match):
        spell = match.group(1);

        self.broker.publish("spellCast", { "spell": spell });


    ################################################################################
    # Method that exists to raise events when a spell fails.
    ################################################################################
    def onSpellFail(self, match):
        spell = match.group(1);

        self.broker.publish("spellFail", { "spell": spell });

    ################################################################################
    # Method that exists to raise events when a spell is targeted.
    ################################################################################
    def onSpellTarget(self, match):
        name = match.group(1);
        target = match.group(2);

        self.broker.publish("spellTarget", { "caster": name, "target": target });


    ################################################################################
    # Method that exists to raise events when someone is advanced.
    ################################################################################
    def onAdvance(self, match):
        advancer = match.group(1);
        advancee = match.group(2);

        self.broker.publish("combatAdvance", { "advancer": advancer, "advancee": advancee });


    ################################################################################
    # Method that exists to raise events when someone retreats.
    ################################################################################
    def onRetreat(self, match):
        retreater = match.group(1);

        self.broker.publish("combatRetreat", { "retreater": retreater });


    ################################################################################
    # Method that exists to raise events when an enemy spawns.
    ################################################################################
    def onEnemySpawn(self, match):
        name = match.group(1);

        self.broker.publish("enemySpawn", { "enemy": name });


    ################################################################################
    # Method that exists to raise events when an enemy is killed.
    ################################################################################
    def onEnemyKill(self, match):
        name = match.group(1);

        self.broker.publish("enemyKill", { "enemy": name });


    ################################################################################
    # Method that exists to raise events when there is a delay
    ################################################################################
    def onDelay(self, match):
        time = match.group(1);

        self.broker.publish("delay", { "time": time });


    ################################################################################
    # Method that exists to raise events when the delay is expired.
    ################################################################################
    def onDelayExpired(self):
        self.broker.publish("delayExpired");

        self.delayTimer = None;

    ################################################################################
    # Method that exists to raise events when the player quits
    ################################################################################
    def onQuit(self, match):
        self.broker.publish("quit");