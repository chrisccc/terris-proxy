import attributes;
import affects;
import magic;
import combat;
import room;

class Player:
    def __init__(self, connection):
        self.connection = connection;

        self.logged_in = False;
        self.name = None;

        self.attributes = attributes.Attributes();
        self.affects = affects.Affects();
        self.magic = magic.Magic();
        self.combat = combat.Combat();
        self.room = room.Room();

    def do(self, command):
        if self.logged_in is False:
            print("Player::do(command = {0}) - Not logged in".format(command));
        else:
            self.connection.sendToClient("^WJarvis>^Y {0}^N\n".format(command));
            self.connection.sendToHost(command + '\n');

    def sendTell(self, what):
            self.connection.sendToClient("[t]^CJarvis^N tells you, \"{0}\".\n^N".format(what));

    def isReady(self):
        return self.logged_in is True;

    def process(self, event):
        topic = event["topic"];
        arguments = event["arguments"];

        if topic is "login" and self.logged_in is False:
            self.logged_in = True;
            self.name = arguments["name"];
            self.magic.name = arguments["name"];

        if topic is "quit":
            self.logged_in = False;

        self.attributes.update(topic, arguments);
        self.affects.update(topic, arguments);
        self.magic.update(topic, arguments);
        self.combat.update(topic, arguments);
        self.room.update(topic, arguments);