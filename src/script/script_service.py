import os.path;
import imp;
import threading;
import time;

import script;

class ScriptService:

    ################################################################################
    # ScriptService constructor.  Exists to initialise the object for use and store
    # references to objects it'll need in the future.
    ################################################################################
    def __init__(self, config, player):
        self.config = config;
        self.player = player;

        self.running = False;
        self.script = None;
        self.script_thread = None;


    ################################################################################
    # Start the service, this exists to create the thread that scripts will run in.
    ################################################################################
    def start(self):
        self.running = True;

        self.script_thread = self.makeScriptThread();
        self.script_thread.start();


    ################################################################################
    # Stop the service, this exists to allow us to stop the current script.
    ################################################################################
    def stop(self):
        self.running = False;

        self.script_thread.join();
        self.script_thread = None;


    ################################################################################
    # Attempts to load a named script module from the path defined in the config.
    # This exists so we can load and run scripts at runtime.
    ################################################################################
    def load(self, module_name):
        full_path = "{0}/{1}.py".format(self.config.script_path, module_name);

        if os.path.isfile(full_path):
            module = imp.load_source(module_name, full_path);

            self.script = script.Script(module);


    ################################################################################
    # Factory method that exists to create and return a thread object.  Exists so
    # as to keep the logic in a single place.
    ################################################################################
    def makeScriptThread(self):
        return threading.Thread(target = self.scriptThread);


    ################################################################################
    # Method that will be ran in a script to contionously call the script process
    # method.  Exists so that the current script may be run.
    ################################################################################
    def scriptThread(self):
        while self.running:
            if self.script:
                self.script.process(self.player);

            time.sleep(0.5);