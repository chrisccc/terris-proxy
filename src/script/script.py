class Script:

    ################################################################################
    # Initialise the script object.  This exists so we can prepare the script object
    # for use.
    ################################################################################
    def __init__(self, module):
        self.module = module;


    ################################################################################
    # Attempts to run the script loaded by this object.  This exists so we can run
    # an external script.
    ################################################################################
    def process(self, player):
        if player.logged_in:
            if self.module:
                self.module.run(player);