import Queue;

import time;
import threading;

import config;

from server import proxy_server;

from terris import terris_processor;
from terris import player;

from script import script_service;

from messaging import broker;

class Application:

    ################################################################################
    # Application constructor.  Creates the event queue so it may be used throughout
    # the object and across threads.
    ################################################################################
    def __init__(self):
        self.events = Queue.Queue();


    ################################################################################
    # Starts the application.  Exists to collect necesasry resources and keep the
    # application running while we're in a valid state.
    ################################################################################
    def start(self):
        print("Starting Terris proxy server")

        broker = self.makeBroker();
        broker.subscribe(self.eventCallback);

        config        = self.makeConfig();
        connection    = self.makeConnection();
        player        = self.makePlayer(connection);
        processor     = self.makeProcessor(self.makeCommands(player), broker);
        scriptService = self.makeScriptService(config, player);
        networkThread = self.makeNetworkThread(connection, processor);

        networkThread.start();

        scriptService.start();

        while networkThread.is_alive():
            try:
                player.process(self.events.get(True, 1));

                self.events.task_done();
            except Queue.Empty as e:
                pass;

        networkThread.join();
        scriptService.stop();

        print("Stopping Terris proxy server");


    ################################################################################
    # Factory method creates and return broker object so as to keep start method
    # as simple as possible.
    ################################################################################
    def makeBroker(self):
        return broker.Broker();


    ################################################################################
    # Factory method creates and return config object so as to keep start method
    # as simple as possible.
    ################################################################################
    def makeConfig(self):
        return config.Config();


    ################################################################################
    # Factory method creates and return script service object so as to keep start
    # method as simple as possible.
    ################################################################################
    def makeScriptService(self, config, player):
        return script_service.ScriptService(config, player);


    ################################################################################
    # Factory method creates and return connection object so as to keep start method
    # as simple as possible.
    ################################################################################
    def makeConnection(self):
        server = proxy_server.ProxyServer("localhost", 8000);
        server.bind()

        connection = None;

        # Wait for our client to connect
        while server.isOpen():
            connection = server.accept();

            if connection is not None:
                server.close();

        return connection;


    ################################################################################
    # Factory method creates and returns player object so as to keep start method as
    # simple as possible.
    ################################################################################
    def makePlayer(self, connection):
        return player.Player(connection);


    ################################################################################
    # Factory method creates and returns commands so as to keep start method as
    # simple as possible.
    ################################################################################
    def makeCommands(self, player):
        return {};


    ################################################################################
    # Factory method creates and returns processor so as to keep start method as
    # simple as possible.
    ################################################################################
    def makeProcessor(self, commands, broker):
        return terris_processor.TerrisProcessor(broker, commands);


    ################################################################################
    # Factory method creates and returns network thread so as to keep start method
    # as simple as possible.
    ################################################################################
    def makeNetworkThread(self, connection, processor):
        return threading.Thread(target = self.networkThread, args = ( connection, processor ));


    ################################################################################
    # Threaded method that will contionously cause both host and client to be read
    # from and their output forwarded to the other end so as to meet the requirements
    # of a proxy server. Additionally, a processor examines input so as to raise
    # game related events we're interested in.
    ################################################################################
    def networkThread(self, connection, processor):
        while connection.isOpen():
            connection.process(processor);


    ################################################################################
    # Exists so as to enqueue events from the processor in a thread safe way that
    # can be safely processed.
    ################################################################################
    def eventCallback(self, topic, arguments):
        self.events.put({"topic": topic, "arguments": arguments});


################################################################################
# Python boiler plate.  Instructs this file what to do if it's run.
################################################################################
if __name__ == "__main__":
    application = Application();
    application.start();



