class Broker:
    def __init__(self):
        self.subscribers = [];

    def subscribe(self, callback):
        self.subscribers.append(callback);

    def unsubscribe(self, callback):
        self.subscribers.remove(callback);

    def publish(self, topic, arguments = None):
        for callback in self.subscribers:
            callback(topic, arguments);