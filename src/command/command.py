class Command:
    def __init__(self, player, name):
        self.player = player;
        self.name = name;

    # Override
    def execute(self):
        pass;
