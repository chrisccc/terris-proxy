import socket;
import proxy_client;

class ProxyServer:
    def __init__(self, address, port):
        self.address = address
        self.port = port
        self.open = False;

        self.server = None

    def isOpen(self):
        return self.open;

    def bind(self):
        try:
            self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM);
            self.server.bind((self.address, self.port));
            self.server.settimeout(0.1);
            self.server.listen(1);
            self.open = True;

            print("Server bound on {0}:{1}".format(self.address, self.port));
        except Exception as e:
            print("ProxyServer::bind() - ", e);

    def accept(self):
        try:
            connection, address = self.server.accept();
            connection.settimeout(0.1);

            client = proxy_client.ProxyClient(connection, address);

            return client;
        except socket.timeout as e:
            pass;
        except Exception as e:
            print("ProxyServer::accept() - ", e);

    def close(self):
        if not self.server is None:
            self.server.close();
            self.open = False;
        else:
            print("ProxyServer::close() - Attempting to close when server doesn't appear to be opened");