import socket;

class SOCKSHandler:
    def __init__(self):
        self.SOCKS_SUCCESS = str(bytearray([0, int("0x5a", 16), 0, 0, 0, 0, 0, 0]));
        self.SOCKS_FAILURE = str(bytearray([0, int("0x5b", 16), 0, 0, 0, 0, 0, 0]))

    def connectToHost(self, data):
        if len(data) < 8:
            print("SOCKSHandler::process() - Data provided did not have minimum 8 bytes: {0}".format(map(ord, data)));
            return

        version = ord(data[0]);
        operation = ord(data[1]);
        # port = socket.ntohl(ord(data[2]) + ord(data[3]));
        port = 31000;
        address = socket.inet_ntoa(data[4:8]);

        if operation != 1:
            print("SOCKSHandler::process() - Requested to perform an invalid operation: {0}".format(operation));
            return

        print("CONNECT request: SOCKS version {0}, port: {1}, address: {2}".format(version, port, address));

        try:
            host = socket.socket(socket.AF_INET, socket.SOCK_STREAM);
            host.connect((address, port))
            host.settimeout(0.1);

            return host;

        except Exception as inst:
            print("Exception: {0}: {1}".format(type(inst), inst));

            return;

    def getSocksSuccess(self):
        return self.SOCKS_SUCCESS;

    def getSocksFailure(self):
        return self.SOCKS_FAILURE;