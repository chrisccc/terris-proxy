import socket;
import socks_handler;

# A proxy client is responsible for proxying data between a client and a host
# socket destination.
class ProxyClient:
    def __init__(self, client, address):
        self.client = client;

        self.host = None;
        self.address = address;
        self.open = True;
        self.state = self.negotiateSocks;

    def isOpen(self):
        return self.open;

    def shutdown(self):
        self.open = False;

        self.client.close();
        self.host.close();

    def process(self, processor):
        self.state(processor);

    def negotiateSocks(self, processor):
        socks = socks_handler.SOCKSHandler();
        self.host = socks.connectToHost(self.readFromClient());

        if self.host:
            self.sendToClient(socks.getSocksSuccess())
            self.state = self.proxyData;
        else:
            sendToClient(socks.getSocksFailure());
            self.shutdown();

    def proxyData(self, processor):
        self.sendToHost(processor.onClientToHost(self.readFromClient()));
        self.sendToClient(processor.onHostToClient(self.readFromHost()));

    def readFromClient(self):
        return self.tryReceive(self.client);

    def readFromHost(self):
        return self.tryReceive(self.host);

    def tryReceive(self, sock):
        try:
            data = sock.recv(4096);

            if data is None or len(data) == 0:
                print("ProxyClient::tryReceive() - Connection closed, shutting down");

                self.shutdown();

                return;

            return data;
        except socket.timeout as e:
            pass;
        except Exception as e:
            print("ProxyClient::tryReceive()", e);

    def sendToClient(self, data):
        self.trySend(self.client, data);

    def sendToHost(self, data):
        self.trySend(self.host, data);

    def trySend(self, sock, data):
        try:
            if data is not None:
                sock.sendall(data);
        except socket.timeout as e:
            pass;
        except Exception as e:
            print("ProxyClient::trySend() - Connection closed, shutting down", e);

            self.shutdown();